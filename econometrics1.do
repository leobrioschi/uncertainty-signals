* Set the working directory

	capture log close
	cd "C:\Users\20201DCCA003\Desktop"
	log using econometria1.txt , replace
	
* Install esttab
	*quietly net install st0085_2.pkg

* Install graph styles

	*quietly ssc install blindschemes	

* Load excel database - variable names' are bigger than supported so collapses to column letters

	import excel "stata-econometria1.xlsx", sheet("Planilha3") firstrow case(low) clear

	
******* operational variables

*> Generating Datetype

	egen date_f = concat(b a),punct("-")
	gen date = monthly(date_f,"YM")
	drop date_f 
	format date %tm
	label var date "Date"
	
*> Some Renaming

	rename ci epu 
	label var epu "Economic Policy Uncertainty Index"
	
	rename a month
	rename b yr
	rename c open
	rename d close
	label var close "IBOV close"
	rename e vol
	rename f qnegs
	rename g max
	rename h min
	rename i media
	rename j num_ipo
	label var num_ipo "# of IPOs"
	rename k delta_ipo
	label var delta_ipo "First day price delta for IPOs"
	
	rename l qtr
	label var qtr quarter
	
	rename m cmax_12
	label var cmax_12 "CMax on t = 12 mo"
	
	rename n cmax_24
	label var cmax_24 "CMax on t = 24 mo"
	
	rename o t25pct_c12
	label var t25pct_c12 "25% cut with CMax_12"

	rename p t30pct_c12
	label var t30pct_c12 "30% cut with CMax_12"	

	rename q t35pct_c12
	label var t35pct_c12 "35% cut with CMax_12"
	
	rename r t25pct_c24
	label var t25pct_c24 "25% cut with CMax_24"

	rename s t30pct_c24
	label var t30pct_c24 "30% cut with CMax_24"	

	rename t t35pct_c24
	label var t35pct_c24 "35% cut with CMax_24"

	local i = 1
	foreach x of varlist u-z aa-av {
	    local name = "jeb_" + "`i'"
		rename `x' `name'
		local ++i
	}

	local i = 1
	foreach x of varlist aw-ch {
	    local name = "gem_" + "`i'"
		rename `x' `name'
		local ++i
	}
	
	macro drop i
	
	label var jeb_1 "Cross-border loans from BIS reporting banks"
	label var jeb_2 "Cross-border loans from BIS banks to nonbanks"
	label var jeb_3 "Official bilateral loans, total"
	label var jeb_4 "Oficial bilateral loans, aid"
	label var jeb_5 "Official bilateral loans, other"
	label var jeb_6 "Multilateral loans, total"
	label var jeb_7 "Multilateral loans, IMF"
	label var jeb_8 "Multilateral loans, other institutions"
	label var jeb_9 "Insured export credit exposures, Berne Union"
	label var jeb_10 "Insured export credit exposures, short term (BU)"
	label var jeb_11 "SDR allocation"
	label var jeb_12 "Liabilities to BIS banks, short"
	label var jeb_13 "Multilateral loans, IMF, short term"
	label var jeb_14 "Debt securities held by nonresidents"
	label var jeb_15 "Debt securities held by nonresidents, total short term"
	label var jeb_16 "Int. debt securities, all maturities"
	label var jeb_17 "Int. debt securities, nonbanks"
	label var jeb_18 "Int. debt securities, short term"
	label var jeb_19 "Int. debt securities, nonbanks short term"
	label var jeb_20 "Paris Club claims (QDA)"
	label var jeb_21 "Paris Club claims (non QDA)"
	label var jeb_22 "Liabilities to BIS banks, locational, total"
	label var jeb_23 "Liabilities to BIS banks, consolidated, total"
	label var jeb_24 "Int. reservers (excluding gold)"
	label var jeb_25 "SDR holdings"
	label var jeb_26 "Portfolio investment assets"
	label var jeb_27 "Cross-border depositis with Bis rep. banks"
	label var jeb_28 "Cross-border dep. with BIS banks, nonbanks"
	
	label var gem_1 "Core CPI, not seas adj."
	label var gem_2 "Core CPI, seas. adj."
	label var gem_3 "CPI price, %y-o-y, median weighted, seas. adj."
	label var gem_4 "CPI price, %y-o-y, not seas. adj."
	label var gem_5 "CPI Price, seas. adj."
	label var gem_6 "CPI Price not seas. adj."
	label var gem_7 "Exchange rate, new LCU\USD ext. back. p. avg."
	label var gem_8 "Exchange rate, old LCU\USD ext. back. p. avg."	
	label var gem_9 "Exports, const. USD, M, not seas. adj."
	label var gem_10 "Exports, const. USD, M, seas. adj."	
	label var gem_11 "Exports, current. USD, M, not seas. adj."	
	label var gem_12 "Exports, current. USD, M, seas. adj."
	label var gem_13 "Exports, price, USD, M, not seas. adj."
	label var gem_14 "Exports, price, USD, M, seas. adj."
	label var gem_15 "Qtr GDP, const. 2010 LCU, seas. adj."
	label var gem_16 "Qtr GDP, const. 2010 USD, seas. adj."
	label var gem_17 "Qtr GDP, current. 2010 LCU, seas. adj."
	label var gem_18 "Qtr GDP, current. 2010 USD, seas. adj."	
	label var gem_19 "Imports, const. USD, M, not seas. adj."
	label var gem_20 "Imports, const. USD, M, seas. adj."	
	label var gem_21 "Imports, current. USD, M, not seas. adj."	
	label var gem_22 "Imports, current. USD, M, seas. adj."
	label var gem_23 "Imports, price, USD, M, not seas. adj."
	label var gem_24 "Imports, price, USD, M, seas. adj."
	label var gem_25 "Industrial prod. USD, seas. adj."
	label var gem_26 "Industrial prod. USD."
	label var gem_27 "J.P. Morgan Emerging M. Bond I (EMBI+)"
	label var gem_28 "J.P. Morgan Emerging M. Bond Spread (EMBI+)"
	label var gem_29 "Months Import Cover of Foreign Reserves"
	label var gem_30 "Nominal Effective Exchange Rate"
	label var gem_31 "Official exchange rate, LCU per USD, avg."
	label var gem_32 "Real effective Exchange Rate"
	label var gem_33 "Retail Sales Volume Index"
	label var gem_34 "Stock Markets LCU"
	label var gem_35 "Stock Markets USD"
	label var gem_36 "Terms of trade"
	label var gem_37 "Total Reserves"
	label var gem_38 "Unemployement rate, %"
	
	gen lnvol = ln(vol)

	gen lnnum_ipo = ln(num_ipo)


	gen lndelta_ipo = ln(delta_ipo)

	
*> Defining macro lists

	* CMax signals, 25%_12t , 30%_12t , 35%_12t, 25%_24t , 30%_24t , 35%_24t respectively.
		unab signals : t25pct_c12 t30pct_c12 t35pct_c12 t25pct_c24 t30pct_c24 t35pct_c24

******* Plotting database
	*ssc install blindschemes if plottig ;plotplain are not available
	set scheme plotplain

	foreach x in `signals' {
		graph twoway (line close date, yaxis(2)) (line `x' date, yaxis(1)), ytitle("", axis(1)) ytitle("", axis(2)) ylabel(1 " ", axis(1)) ylabel(0 " " 50000 "{bf:50k pts.}" 100000 "{bf:100k pts.}" 127000 "{bf:127k. pts}", axis(2)) xtitle("{stMono:Time frame}") legend(ring(00) position(10) bmargin(large)) caption("{bf:Source}: {it:Author}, based on publicly available data")

		*graph save `x'-graph
		graph export `x'-graph.png
	}
	
	graph twoway (line close date, yaxis(2)) (line epu date, yaxis(1)), ytitle("", axis(1)) ytitle("", axis(2)) xtitle("{stMono:Time frame}") ylabel(0 " " 50000 "{bf:50k pts.}" 100000 "{bf:100k pts.}" 127000 "{bf:127k. pts}", axis(2)) legend(ring(00) position(10) bmargin(large)) caption("{bf:Source}: {it:Author}, based on publicly available data")
	
	graph export epu.png
	
	estpost tabstat close epu vol delta_ipo jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992, stat(n min p25 mean p50 sd p75 max) columns(statistics)
	
	esttab . using "stats.tex", cells("n min p25 mean p50 sd p75 max") title("Summary statistics") tex wide alignment(cr) replace

* The one year time delta for CMax is less noisy.
		unab model : t25pct_c12 t30pct_c12 t35pct_c12 
		
*** Regression

	foreach x in `model'{
		reg `x' jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store ols_coef

		logit `x' jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store logit_coef
		quietly margins, dydx(*) post
		est store logit_ape

		probit `x' jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store probit_coef
		quietly margins, dydx(*) post
		est store probit_ape
		
		esttab ols_coef logit_coef probit_coef using "table1.tex", mtitles("OLS" "LOGIT" "PROBIT" ) star(* 0.1 ** 0.05 *** 0.01) title("Estimation results with macro data for `x'") tex wide alignment(cr) append r2 pr2
		
		esttab ols_coef logit_ape probit_ape using "table1.tex", mtitles("OLS"  "LOGIT-APE" "PROBIT-APE") star(* 0.1 ** 0.05 *** 0.01) title("Coefficients comparative with macro data for `x'") tex wide alignment(cr) append r2 pr2
	}
	
	*set trace on
	foreach x in `model'{
		reg `x' epu vol delta_ipo jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store ols_coef

		logit `x' epu vol delta_ipo jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store logit_coef
		quietly margins, dydx(*) post
		est store logit_ape

		probit `x' epu vol delta_ipo jeb_24 jeb_17 gem_5 gem_16 gem_25 gem_28 gem_32 if yr > 1992,rob
		est store probit_coef
		quietly margins, dydx(*) post
		est store probit_ape
	
		esttab ols_coef logit_coef  probit_coef  using "table2.tex", mtitles("OLS" "LOGIT" "PROBIT" ) star(* 0.1 ** 0.05 *** 0.01) title("Estimation results with macro and sentiment data for `x'") tex wide alignment(cr) append r2 pr2
		
		esttab ols_coef logit_ape  probit_ape using "table2.tex", mtitles("OLS" "LOGIT-APE" "PROBIT-APE") star(* 0.1 ** 0.05 *** 0.01) title("Coefficients comparative with macro and sentiment data for `x'") tex wide alignment(cr) append r2 pr2

	}

log close