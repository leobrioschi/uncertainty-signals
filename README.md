# Can uncertainty signals from the policy-maker infer stock market crises? A Brazilian anecdote.

Code and original working paper by Leonardo Brioschi

## Abstract

Uncertainty around economic policy keeps economic agents on their toes, this study shows that this uncertainty can help predict crises periods on the Brazilian stock market, measured by the Bovespa Index and tested with a simple binary model with logit and probit  comparisons. The relationship that comes from the results is that plugging holes in a bearish market can be one of the factors to unleash a market crisis.

## Keywords
- Behavioral Finance (G40)
- Information and Market Eciency (G14)
- Financial Markets (G10).

## Usage
The file **econometrics1.do** contains the stata *do-file* used in the working paper. Data is extracted from *ECONOMÁTICA*, transformed in an *excel worksheet* and merged with data from the *World Bank Database* resulting in *stata-econometria1.xlsx*, the database used in the study. All data used is discriminated inside the working paper.

The **.pdf** in this repo contains the last publicly available working paper, with its' corresponding LaTeX file (**.tex**) and used figures are in the *figures/* folder.

## Support
No support is provided, but any questions can be forwarded to my e-mail leobrioschi [at] proton [dot] me

## Contributing
I would gladly recieve any contribution and expand the study with other authors, just get in touch! 
- leobrioschi [at] proton [dot] me

## Authors and acknowledgment
Leonardo Brioschi, PhD. Candidate at FUCAPE Business School - Accounting/Finance

## License
This state of the working paper follows the UNLICENSE scheme.

## Project status
On Halt due to other working papers and activities taking a bit of my free time.
